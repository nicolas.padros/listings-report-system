resolvers += Resolver.typesafeRepo("snapshots")

resolvers += Resolver.jcenterRepo

// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.7")

//Docker Plugin
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.7.6")
