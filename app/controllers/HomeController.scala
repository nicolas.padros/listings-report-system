package controllers

import akka.stream.IOResult
import akka.stream.scaladsl._
import akka.util.ByteString
import business.FilesAnalyzer
import business.strategy.impl.ReportMaker
import com.github.tototoshi.csv.CSVReader
import play.api._
import play.api.libs.json.Json
import play.api.libs.streams._
import play.api.mvc.MultipartFormData.FilePart
import play.api.mvc._
import play.core.parsers.Multipart.FileInfo

import java.io.File
import java.nio.file.{Files, Path}
import java.text.SimpleDateFormat
import javax.inject._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

case class FormData(name: String)

/**
 * This controller handles a file upload.
 */
@Singleton
class HomeController @Inject() (cc:MessagesControllerComponents)
                               (implicit executionContext: ExecutionContext)
  extends MessagesAbstractController(cc) {

  private val logger = Logger(this.getClass)

  private val listingsReader = CSVReader.open(new File("public/listings.csv"))
  private val contactsReader = CSVReader.open(new File("public/contacts.csv"))

  private var reportMaker = new ReportMaker(new FilesAnalyzer(listingsReader.all(), contactsReader.all()))

  type Table = List[List[String]]

  type FilePartHandler[A] = FileInfo => Accumulator[ByteString, FilePart[A]]

  /**
   * Uses a custom FilePartHandler to return a type of "File" rather than
   * using Play's TemporaryFile class.  Deletion must happen explicitly on
   * completion, rather than TemporaryFile (which uses finalization to
   * delete temporary files).
   *
   * @return
   */
  private def handleFilePartAsFile: FilePartHandler[File] = {
    case FileInfo(partName, filename, contentType, _) =>
      val path: Path = Files.createTempFile("multipartBody", "tempFile")
      val fileSink: Sink[ByteString, Future[IOResult]] = FileIO.toPath(path)
      val accumulator: Accumulator[ByteString, IOResult] = Accumulator(fileSink)
      accumulator.map {
        case IOResult(count, status) =>
          logger.info(s"count = $count, status = $status")
          FilePart(partName, filename, contentType, path.toFile)
      }
  }


  /**
   * Validates the file given the criteria
   * @param file to be validated
   * @return Some(table) if success, None otherwise
   */
  private def validateListingsFile(file: File): Option[Table] = {
    val reader: Table = CSVReader.open(file).all()
    reader.headOption match {
      case Some(headers: List[String]) => headers match {
        case "id" :: "make" :: "price" :: "mileage" :: "seller_type" :: Nil =>
          if(reader.tail.foldLeft(true){
            (validationStatus, row: List[String]) => {
              validationStatus && row.head.forall(_.isDigit) && row(2).forall(_.isDigit) && row(3).forall(_.isDigit)
            }
          }) Some(reader) else None
        case ::(head, next) => None
        case Nil => None
      }
      case None => None
    }

  }

  /**
   * Validates the file given the criteria
   * @param file to be validated
   * @return Some(table) if success, None otherwise
   */
  private def validateContactsFile(file: File): Option[Table] = {
    val reader: Table = CSVReader.open(file).all()
    val format = new SimpleDateFormat("YYYY")
    reader.headOption match {
      case Some(headers: List[String]) => headers match {
        case "listing_id" :: "contact_date" :: Nil =>
          if(reader.tail.foldLeft(true) {
            (validationStatus, row: List[String]) => {
              validationStatus && row.head.forall(_.isDigit) && Try(format.parse(row(1))).isSuccess
            }
          }) Some(reader) else None
        case ::(head, next) => None
        case Nil => None
      }
      case None => None
    }
  }

  /**
   * Uploads both CSV files in a single POST request.
   *
   * @return
   */
  def upload: Action[MultipartFormData[File]] = Action.async(parse.multipartFormData(handleFilePartAsFile)) { implicit request =>
    Future {

      //validates the Listing in the body
      val listingStatus: Option[Table] = request.body.file("listings").flatMap {
        case FilePart(key, filename, contentType, file, fileSize, dispositionType) =>
          logger.info(s"key = $key, filename = $filename, contentType = $contentType, file = $file, fileSize = $fileSize, dispositionType = $dispositionType")
          val status = validateListingsFile(file)
          status
      }

      //validates the Contacts File in the body
      val contactsStatus: Option[Table] = request.body.file("contacts").flatMap {
        case FilePart(key, filename, contentType, file, fileSize, dispositionType) =>
          logger.info(s"key = $key, filename = $filename, contentType = $contentType, file = $file, fileSize = $fileSize, dispositionType = $dispositionType")
          val status = validateContactsFile(file)
          status
      }

      listingStatus match {
        case Some(listings: Table) => contactsStatus match {
          case Some(contacts: Table) =>
            this.reportMaker = new ReportMaker(new FilesAnalyzer(listings, contacts)) //now we start using the uploaded files
            Ok(Json.toJson("Validation passed, now using uploaded files"))
          case None => BadRequest(Json.toJson("Validation failed for contacts.csv"))
        }
        case None => BadRequest(Json.toJson("Validation failed for listings.csv"))
      }
    }
  }



  /**
   * Gets the average listing grouped by seller types
   *
   * @return a map containing the result
   */
  def getAverageListingForSellerType = Action.async {
    Future(Ok(Json.toJson(reportMaker.getAverageListingForSellerType)))
  }

  /**
   * Gets the Percentual Distribution of available cars by Make
   *
   * @return a map containing the result
   */
  def getPercentualDistributionByMake = Action.async {
    Future(Ok(Json.toJson(reportMaker.getPercentualDistributionByMake)))
  }

  /**
   * Gets the Average price for most contacted listingsWithHeaders, given the percentage
   *
   * @param percentage representing the top %
   * @return The average price
   */
  def getAvgPriceForMostContacted(percentage: Int) = Action.async {
    Future(Ok(Json.toJson(reportMaker.getAvgPriceForMostContacted(percentage))))
  }

  /**
   * Gets the Top 5 Most contacted Listings per Month
   *
   * @return a Report
   */
  def getTop5MostContactedPerMonth = Action.async {
    Future(Ok(Json.toJson(reportMaker.getTop5MostContactedPerMonth)))
  }

}
