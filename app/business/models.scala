package business

import play.api.libs.json.{Json, OFormat}

case class AverageListing(sellerType: String, averageInEuro: String)

object AverageListing {
  implicit val format: OFormat[AverageListing] = Json.format[AverageListing]
}

case class PercentualDistribution(make: String, distribution: String)

object PercentualDistribution {
  implicit val format: OFormat[PercentualDistribution] = Json.format[PercentualDistribution]
}

case class AveragePriceForMostContacted(averagePrice: String)

object AveragePriceForMostContacted {
  implicit val format: OFormat[AveragePriceForMostContacted] = Json.format[AveragePriceForMostContacted]
}



case class ListingWithContactWithRanking(ranking: String, listingId: String, make: String, sellingPrice: String,
                                         mileage: String, totalAmountOfContacts: String)

object ListingWithContactWithRanking {
  implicit val format: OFormat[ListingWithContactWithRanking] = Json.format[ListingWithContactWithRanking]
}


case class TopFiveMostContactedListingsReport(month: String, listings: Iterable[ListingWithContactWithRanking])

object TopFiveMostContactedListingsReport {
  implicit val format: OFormat[TopFiveMostContactedListingsReport] = Json.format[TopFiveMostContactedListingsReport]
}

