package business.strategy.model

import business.{AverageListing, AveragePriceForMostContacted, PercentualDistribution, TopFiveMostContactedListingsReport}

import java.util.Calendar

/**
 * This trait returns all the required reports in JSON-printable form
 */
trait Reporter {

  /**
   * Gets the average listing grouped by seller types
   *
   * @return a map containing the result
   */
  def getAverageListingForSellerType: Iterable[AverageListing]

  /**
   * Gets the Percentual Distribution of available cars by Make
   *
   * @return a map containing the result
   */
  def getPercentualDistributionByMake: Iterable[PercentualDistribution]

  /**
   * Gets the Average price for most contacted listingsWithHeaders, given the percentage
   *
   * @param percentage representing the top %
   * @return The average price
   */
  def getAvgPriceForMostContacted(percentage: Int): AveragePriceForMostContacted

  /**
   * Gets the Top 5 Most contacted Listings per Month
   *
   * @return a Report
   */
  def getTop5MostContactedPerMonth: Iterable[TopFiveMostContactedListingsReport]
}
