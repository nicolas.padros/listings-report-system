package business.strategy.impl

import business._
import business.strategy.model.Reporter

import java.text.SimpleDateFormat
import java.util.Calendar
import scala.collection.immutable


/**
 * This class uses the FilesAnalyzer to make it compatible to JSON
 * @param analyzer to create all the reports
 */
class ReportMaker(analyzer: FilesAnalyzer) extends Reporter {
  /**
   * Gets the average listing grouped by seller types
   *
   * @return a map containing the result
   */
  def getAverageListingForSellerType: Iterable[AverageListing] = analyzer.getAverageListingForSellerType.map(row => AverageListing(row._1, s"€${row._2},-"))

  /**
   * Gets the Percentual Distribution of available cars by Make
   *
   * @return a map containing the result
   */
  def getPercentualDistributionByMake: Iterable[PercentualDistribution] = analyzer.getPercentualDistributionByMake.map{
    case (make, distribution) => PercentualDistribution(make, s"$distribution%")
  }

  /**
   * Gets the Average price for most contacted listingsWithHeaders, given the percentage
   *
   * @param percentage representing the top %
   * @return The average price
   */
  def getAvgPriceForMostContacted(percentage: Int): AveragePriceForMostContacted =
    AveragePriceForMostContacted(s"€${analyzer.getAvgPriceForMostContacted(percentage)},-")

  /**
   * Gets the Top 5 Most contacted Listings per Month
   *
   * @return a Report
   */
  def getTop5MostContactedPerMonth: Iterable[TopFiveMostContactedListingsReport] = {
    val dateFormat = new SimpleDateFormat("MM.yyyy")
    val a = analyzer.getTop5MostContactedPerMonth
      a
      .map{
      case (calendar: Calendar, value: List[(List[String], Int)]) =>
        TopFiveMostContactedListingsReport(dateFormat.format(calendar.getTime), value.map{
          case (listing: Seq[String], index: Int) =>
            ListingWithContactWithRanking((index+1).toString,
            listing(1),
            listing(2),
              s"€${listing(3)}-,",
            s"${listing(4)} KM",
            listing.head
          )
        })
    }
  }
}

