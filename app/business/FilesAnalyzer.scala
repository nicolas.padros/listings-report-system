package business

import java.util.Calendar

/**
 * This class makes all the required computations and returns them in plain String structures
 * @param listingsWithHeaders all listings
 * @param contactsWithHeaders all contacts
 */
class FilesAnalyzer(listingsWithHeaders: List[List[String]], contactsWithHeaders: List[List[String]]) {

  private val listingsContent: List[List[String]] = listingsWithHeaders.tail
  private val contactsContent: List[List[String]] = contactsWithHeaders.tail


  type Table = Seq[List[String]]

  /**
   * Gets the average listing grouped by seller types
   *
   * @return a map containing the result
   */
  def getAverageListingForSellerType: Map[String, String] = listingsContent
    .groupBy(_ (4)) //groups by seller type
    .map {
      case (seller, rows) => seller -> (rows.map(_ (2).toInt).sum / rows.length).toString //process the average price
    }

  /**
   * Gets the Percentual Distribution of available cars by Make
   *
   * @return a map containing the result
   */
  def getPercentualDistributionByMake: Map[String, String] = {
    listingsContent
      .groupBy(_ (1)) //group by make
      .map {
        case (make, rows: List[List[String]]) => make -> ((rows.length.toFloat / listingsContent.length.toFloat) * 100).toString //creates the percentage
      }
  }

  /**
   * Gets the Average price for most contacted listingsWithHeaders, given the percentage
   *
   * @param percentage representing the top %
   * @return The average price
   */
  def getAvgPriceForMostContacted(percentage: Int): String = {

    val listingsIdssortedByNumberOfContacts = contactsContent
      .groupBy(_.head) //group by id to find multiple contacts
      .map {
        case (listingId, value) => (List(listingId, value.length.toString)) //maps to number of contacts
      }.toList.sortWith(_(1).toInt > _(1).toInt) //sort in descending order by amount of contacts


    //this gets the top % listings
    val numberOfListingsToTake = (listingsIdssortedByNumberOfContacts.size * (percentage.toFloat / 100f)).toInt
    val mostContactedListings = if(listingsIdssortedByNumberOfContacts.size > 1)
     listingsIdssortedByNumberOfContacts
      .take(numberOfListingsToTake) else listingsIdssortedByNumberOfContacts

    (mostContactedListings
      .map {
        rowWithContacts: List[String] =>
          listingsContent.find(row => rowWithContacts.head == row.head) match { //find the listing by id
            case Some(listing) => listing(2).toInt //get the price
            case None => throw new IllegalStateException(s"No listing found for id: ${rowWithContacts.head} in contactsWithHeaders.csv")
          }
      }.sum / mostContactedListings.size).toString //return the average
  }

  /**
   * Gets the Top 5 Most contacted Listings per Month
   *
   * @return a Report
   */
  def getTop5MostContactedPerMonth: List[(Calendar, List[(List[String], Int)])] = {
    contactsContent.map {
      row: List[String] =>
        val calendar = Calendar.getInstance()
        calendar.setTimeInMillis(row(1).toLong)
        row.head -> calendar //this has the listing_id and the Date in Calendar format
    }
      .groupBy(_._2.get(Calendar.MONTH)) //group by month
      .map {
        case (_, value: List[(String, Calendar)]) => value.head._2 -> value //remove the number of the month
      }
      .toList
      .sortWith {
        case (a, b) => a._1.before(b._1) //sort in chronological order
      }
      .map {
        case (month: Calendar, contactsInMonth: Seq[(String, Calendar)]) => // Month -> (listing_id, amount_of_contacts)
          val value: List[(String, Int)] = contactsInMonth
            .groupBy(_._1) //group by listing_id
            .map {
              case (listingId, rows: Seq[(String, Calendar)]) => listingId -> rows.length //gets the total amount of contacts
            }
            .toList
            .sortWith(_._2 > _._2) //sorted by most contacted first
            .take(5) //gets only the top 5
          month -> value //month -> Top 5 ListingIds
      }.map { // Now we get the listing information
      case (month, listingAndAmount) => month -> listingAndAmount.map {
        case (listing: String, contacts: Int) => listingsContent.find(_.head == listing) match {
          case Some(fullListing: List[String]) => contacts.toString :: fullListing //we find the full listing
          case None => throw new IllegalStateException(s"No listing id found for $listing")
        }
      }
        .zipWithIndex //return with index
    }
  }
}
