lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    name := """listings-report-analyzer""",
    version := "latest",
    scalaVersion := "2.13.1",
    libraryDependencies ++= Seq(
      ws,
      guice,
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test,
      "com.github.tototoshi" %% "scala-csv" % "1.3.6",
      "com.typesafe.play" %% "play-json" % "2.7.4"
    ),
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings"
    )
  )


//Docker

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)
//enablePlugins(AshScriptPlugin)

//Docker Config
dockerBaseImage := "openjdk:8"
dockerExposedPorts ++= Seq(9000)

javaOptions in Universal ++= Seq(
  "-Dpidfile.path=/dev/null"
)

mappings in Universal += file("public/listings.csv") -> "public/listings.csv"
mappings in Universal += file("public/contacts.csv") -> "public/contacts.csv"
