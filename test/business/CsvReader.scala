package business

import com.github.tototoshi.csv.CSVReader
import org.scalatest.{FlatSpec, Matchers}

import java.io.File

class CsvReader extends FlatSpec with Matchers {

  private val listingsReader = CSVReader.open(new File("public/listings.csv"))
  private val contactsReader = CSVReader.open(new File("public/contacts.csv"))

  private val analyzer = new FilesAnalyzer(listingsReader.all(), contactsReader.all())



  it should "print the AverageListingForSellerType" in {

    println(analyzer.getAverageListingForSellerType)
  }

  it should "print the percentual distribution" in {
    println(analyzer.getPercentualDistributionByMake)
  }

  it should "print the avg price for 30% most contacted" in {
    println(analyzer.getAvgPriceForMostContacted(30))
  }

  it should "print the top 5 sellers by month" in {
    val month = analyzer.getTop5MostContactedPerMonth
    println(month)
  }

}
