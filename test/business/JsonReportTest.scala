package business

import business.strategy.impl.ReportMaker
import com.github.tototoshi.csv.CSVReader
import org.joda.time.DateTime
import org.scalatest.{FlatSpec, Matchers}
import play.api.libs.json.Json

import java.io.File
import java.text.SimpleDateFormat
import scala.util.Try

class JsonReportTest extends FlatSpec with Matchers {

  private val listingsReader = CSVReader.open(new File("public/listings.csv"))
  private val contactsReader = CSVReader.open(new File("public/contacts.csv"))

  private var reportMaker = new ReportMaker(new FilesAnalyzer(listingsReader.all(), contactsReader.all()))


  "JsonReport" should "return a pretty result in avg listing" in {
    println(Json.prettyPrint(Json.toJson(reportMaker.getAverageListingForSellerType)))
  }

  it should "print the percentual distribution" in {
    println(Json.prettyPrint(Json.toJson(reportMaker.getPercentualDistributionByMake)))
  }

  it should "print the avg price for 30% most contacted" in {
    println(Json.prettyPrint(Json.toJson(reportMaker.getAvgPriceForMostContacted(30))))
  }

  it should "print the top 5 sellers by month" in {
    println(Json.prettyPrint(Json.toJson(reportMaker.getTop5MostContactedPerMonth)))
  }

  it should "parse a date in String" in {
    val format = new SimpleDateFormat("mm")
    Try(format.parse("1592498493000")).isSuccess shouldBe true
  }

}
