# Listing Reports Backend
This is the backend for the Listing Reports Home Assignment sent by AutoScout24 to Nicolas Padros.


## Frameworks used
This is a standard Play Framework project. It was initialized by a Lightbend Project Starter with Intellij IDEA.
An additional library was used for CSV parsing. Tests were made but they don't assert anything, which is far from ideal.

## Command to run
`sbt run`

## Build Docker image
To build the docker image, run the command `sbt docker:publishLocal`. This will create an image in your local repository.

This was done via the [sbt-native-packager](https://github.com/sbt/sbt-native-packager), which builds the image and creates the Dockerfile by itself.
